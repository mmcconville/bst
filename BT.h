#ifndef H_BT
#define H_BT
#include <iostream>

using namespace std;

template <class T>
struct Node
{
	T info;
	Node<T>* l_child;
	Node<T>* r_child;
};

///////////////
// Definition
///////////////
template <class T>
class BT
{
	private:
		Node<T>* current;
		Node<T>* trail;
		bool is_leaf(const Node<T>* node); // Is node a leaf?
		int height(const Node<T>* node); // Height of tree method
		int node_count(const Node<T>* node); // The actual node count method
		int leaf_count(const Node<T>* node); // The actual leaf count method
		void in_order_traversal(const Node<T>* node); // In-order traversal
		void post_order_traversal(const Node<T>* node); // Post-order traversal
		void pre_order_traversal(const Node<T>* node); // Pre-order traversal
		void delete_tree(Node<T>* node); // Delete tree method
		
	protected:
		Node<T>* root;
	
	public:
		BT(); // default constructor
		~BT(); // destructor
		bool is_empty(); // Is the tree empty?
		int height(); // Height of tree helper method
		int node_count(); // How many nodes? Helper method
		int leaf_count(); // How many leaves? Helper method
		void in_order_traversal(); // In order helper method
		void post_order_traversal(); // Post order helper method
		void pre_order_traversal(); // Pre order helper method
		void delete_tree(); // Delete tree helper method

};


//////////////////
// Implementation
//////////////////

// Default constructor
// input: none
template <class T>
BT<T>::BT(){}

// Destructor
// input: none
template <class T>
BT<T>::~BT()
{
	delete_tree(this->root);
}

// bool method returns whether tree is empty
// input: none
template <class T>
bool BT<T>::is_empty()
{
	return this->root == NULL;
}

// int helper method returns the height of the tree
// input: none
template <class T>
int BT<T>::height()
{
	return height(this->root);
}

// int method returns the height of a tree
// input: root of tree/subtree
template <class T>
int BT<T>::height(const Node<T>* node)
{
	if (node)
	{
		return max(1 + height(node->l_child), 1 + height(node->r_child));
	}
	else
	{
		return 0;
	}
}

// int helper method returns the total node count
// input: none
template <class T>
int BT<T>::node_count()
{
	return node_count(this->root);
}

// bool method returns whether a node is a leaf
// input: a node pointer
template <class T>
bool BT<T>::is_leaf(const Node<T>* node)
{
	return !(node->l_child || node->r_child);
}

// int method that returns the node count of a tree/subtree
// input: root of tree/subtree
template <class T>
int BT<T>::node_count(const Node<T>* node)
{
	if (!node)
	{
		return 0;
	}
	else
	{
		return 1 + node_count(node->l_child) + node_count(node->r_child);
	}
}

// int helper method returns number of leaves
// input: none
template <class T>
int BT<T>::leaf_count()
{
	return leaf_count(this->root);
}

// int method returns number of leaves in a tree/subtree
// input: root of tree/subtree
template <class T>
int BT<T>::leaf_count(const Node<T>* node)
{
	if (!node)
	{
		return 0;
	}
	// If there are children, keep calling recursively until no children
	else if (node->l_child || node->r_child)
	{
		return leaf_count(node->l_child) + leaf_count(node->r_child);
	}
	// If no children, then it's a leaf
	else
	{
		return 1;
	}
}

// Helper method calls in-order traversal on root
// input: none
template <class T>
void BT<T>::in_order_traversal()
{
	in_order_traversal(this->root);
}

// Performs an in-order traversal of a tree
// input: root of tree/subtree
template <class T>
void BT<T>::in_order_traversal(const Node<T>* node)
{
	if (node)
	{
		in_order_traversal(node->l_child);
		cout << node->info << " ";
		in_order_traversal(node->r_child);
	}
}

// Helper method calls post-order traversal on root
// input: none
template <class T>
void BT<T>::post_order_traversal()
{
	post_order_traversal(this->root);
}

// Performs a post-order traversal of a tree
// input: root of tree/subtree
template <class T>
void BT<T>::post_order_traversal(const Node<T>* node)
{
	if (node)
	{
		post_order_traversal(node->l_child);
		post_order_traversal(node->r_child);
		cout << node->info << " ";
	}
}

// Helper method calls pre-order traversal on root
// input: none
template <class T>
void BT<T>::pre_order_traversal()
{
	pre_order_traversal(this->root);
}

// Performs a pre-order traversal of a tree
// input: root of tree/subtree
template <class T>
void BT<T>::pre_order_traversal(const Node<T>* node)
{
	if (node)
	{
		cout << node->info << " ";
		pre_order_traversal(node->l_child);
		pre_order_traversal(node->r_child);
	}
}

// Delete tree helper function
// input: none
template <class T>
void BT<T>::delete_tree()
{
	delete_tree(this->root);
}

// Deletes a tree/subtree
// input: root of tree/subtree
template <class T>
void BT<T>::delete_tree(Node<T>* node)
{
	// Can't delete an empty tree
	if (!node)
	{
		return;
	}
	bool is_root = (node == this->root);
	if (is_leaf(node))
	{
		delete node;
		node = NULL;
	}
	else
	{
		// Delete the left subtree
		if (node->l_child)
			delete_tree(node->l_child);
		node->l_child = NULL;
		// Delete the right subtree
		if (node->r_child)
			delete_tree(node->r_child);
		node->r_child = NULL;
		node = NULL;
		if (is_root)
			this->root = NULL;
		delete node;
	}
}
#endif