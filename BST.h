#ifndef H_BST
#define H_BST
#include "BT.h"

using namespace std;

///////////////
// Definition
///////////////
template <class T>
class BST : public BT<T>
{
	private:
		Node<T>* current;
		Node<T>* trail;
		void del_node(Node<T>* &node);
	
	public:
		BST(); // default, parameter-less constructor
		BST(T item); // single-parameter constructor
		void add_item(const T& item);
		void del_item(const T& item); // Finds the node to delete
};

///////////////////
// Implementation
///////////////////

// Default constructor
// input: none
template <class T>
BST<T>::BST()
{
	this->root = NULL;
}

// Constructor with item to be root
// input: item of type T to be the root of the tree
template <class T>
BST<T>::BST(T item)
{
	// Create a new node
	Node<T>* new_node = new Node<T>;
	new_node->info = item;
	new_node->l_child = NULL;
	new_node->r_child = NULL;
	this->root = new_node;
}

// Adds item of type T to the tree
// input: item to add
template <class T>
void BST<T>::add_item(const T& item)
{
	// Create a new node
	Node<T>* new_node = new Node<T>;
	new_node->info = item;
	new_node->l_child = NULL;
	new_node->r_child = NULL;
	
	// Is this tree empty?
	if (BT<T>::is_empty())
	{
		BT<T>::root = new_node;
	}
	else
	{
		current = this->root;
		trail = this->root;
		while (current)
		{
			trail = current;
			// Tree cannot have duplicate entries
			if (item == current->info)
			{
				cout << "No duplicates\n";
				return;
			}
			else
			{
				if (item < current->info)
				{
					current = current->l_child;
				}
				else
				{
					current = current->r_child;
				}
			}
		}
		if (item < trail->info)
		{
			trail->l_child = new_node;
		}
		else
		{
			trail->r_child = new_node;
		}
	}
}


// This method finds the node containing the item to be deleted if it exists
// input: item to find
template <class T>
void BST<T>::del_item(const T& item)
{
	Node<T>* current = this->root;
	Node<T>* trail = this->root;
	bool found = false;
	
	// Only search if the tree is not empty
	if (this->root)
	{
		while (current != NULL && !found)
		{
			// Node found
			if (current->info == item)
			{
				found = true;
			}
			else
			{
				trail = current;
				// Look left if smaller, else look right
				if (current->info < item)
				{
					current = current->r_child;
				}
				else
				{
					current = current->l_child;
				}
			}
		}
		if (current && found)
		{
			if (current == this->root)
			{
				del_node(this->root);
			}
			else if (trail->info > item)
			{
				del_node(trail->l_child);
			}
			else
			{
				del_node(trail->r_child);
			}
		}
	}
}

// This methods handles the deletion of the node to be removed
// input: node to remove
template <class T>
void BST<T>::del_node(Node<T>* &node)
{
	Node<T>* current;
	Node<T>* trail;
	Node<T>* temp; // The node being deleted
	
	// If node is NULL, do nothing
	if (!node)
	{
		return;
	}
	// If l and r children are NULL, delete node
	else if (!node->l_child && !node->r_child)
	{
		temp = node;
		node = NULL;
		delete temp;
	}
	// If l child is NULL, point to r child instead
	else if (!node->l_child)
	{
		temp = node;
		node = node->r_child;
		delete temp;
	}	
	// If r child is NULL, point to l child instead
	else if (!node->r_child)
	{
		temp = node;
		node = node->l_child;
		delete temp;
	}
	// Need to go left once, then right until NULL
	else
	{
		current = node->l_child;
		trail = NULL;
		
		while (current->r_child)
		{
			trail = current;
			current = current->r_child;
		}
		
		node->info = current->info;
		
		// Trail NULL because it never moved
		if (!trail)
		{
			node->l_child = current->l_child;
		}
		else
		{
			trail->r_child = current->l_child;
		}
		delete current;
	}	
}
#endif
